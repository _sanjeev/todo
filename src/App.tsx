import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Link, Routes, Navigate } from "react-router-dom";
import Login from "./Components/Login";
import ProtectedRoute from "./Components/ProtectedRoute";
import Dashboard from "./Components/Dashboard";
import NotFound from "./Components/NotFound";
import Home from "./Components/Home";

type User = {
    email: string;
    password: string;
};

type AuthContextType = {
    isAuthenticated: boolean;
    login: (user: User) => void;
    logout: () => void;
};

const initialAuthContext: AuthContextType = {
    isAuthenticated: false,
    login: () => {},
    logout: () => {},
};

export const AuthContext = React.createContext<AuthContextType>(initialAuthContext);

const App = () => {
    const [isAuthenticated, setAuthenticated] = useState<boolean>(false);

    const login = () => {
        setAuthenticated(true);
    };

    const logout = () => {
        setAuthenticated(false);
        localStorage.removeItem("token");
    };

    useEffect(() => {
        const token = localStorage.getItem("token");
        if (token) {
            setAuthenticated(true);
        }
    }, []);

    const authContextValue: AuthContextType = {
        isAuthenticated,
        login,
        logout,
    };

    return (
        <Router>
            <AuthContext.Provider value={authContextValue}>
                <div>
                    <nav className="navbar">
                        <ul className="navbar__menu">
                            {!isAuthenticated && (
                                <li className="navbar__item">
                                    <Link to="/login" className="navbar__link">
                                        Login
                                    </Link>
                                </li>
                            )}
                            {isAuthenticated && (
                                <li className="navbar__item" onClick={logout}>
                                    Logout
                                </li>
                            )}
                            <li className="navbar__item">
                                <Link to="/dashboard" className="navbar__link">
                                    Dashboard
                                </Link>
                            </li>
                        </ul>
                    </nav>

                    <Routes>
                        <Route path="/" element={<Home />} />
                        {isAuthenticated ? (
                            <Route path="/login" element={<Navigate to="/dashboard" replace />} />
                        ) : (
                            <Route path="/login" element={<Login setAuthenticated={setAuthenticated} />} />
                        )}
                        <Route path="/dashboard" element={<ProtectedRoute element={<Dashboard />} />} />
                        <Route path="*" element={<NotFound />} />
                    </Routes>
                </div>
            </AuthContext.Provider>
        </Router>
    );
};

export default App;
