import React, { useState } from "react";
import { useTodoContext } from "./TodoContext";
import { v4 as uuid } from "uuid";
import "./TodoList.css";

const TodoList = () => {
    const { todos, addTodo, updateTodo } = useTodoContext();
    const [subTask, setSubTask] = useState<object>({});
    const [title, setTitle] = useState<string>("");

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        const updatedTodo: any = {
            ...subTask,
        };
        updateTodo(updatedTodo);
        setTitle("");
    };

    return (
        <ul className="todo-list">
            {todos.map((todo, index) => (
                <div key={todo.id}>
                    <li className={""}>
                        <span>{todo.title}</span>
                    </li>
                    {todo.subTask.length ? (
                        todo.subTask.map((subTask: any) => (
                            <li className={""} key={subTask.id}>
                                <span>{subTask.title}</span>
                            </li>
                        ))
                    ) : (
                        <></>
                    )}
                    <form onSubmit={handleSubmit}>
                        <input
                            type="text"
                            className="input"
                            // title={title}
                            value={title}
                            placeholder="Add Subtask"
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                                setTitle(e.target.value);
                                setSubTask({
                                    id: todo.id,
                                    title: todo.title,
                                    subTask: [
                                        {
                                            id: uuid(),
                                            title: e.target.value,
                                        },
                                    ],
                                });
                            }}
                        />
                        <input type="submit" value="Add Subtask" />
                    </form>
                </div>
            ))}
        </ul>
    );
};

export default TodoList;
