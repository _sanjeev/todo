import React, { useContext } from "react";
import { Navigate, Route } from "react-router-dom";
import { AuthContext } from "../App";
import "../App.css";

type ProtectedRouteProps = {
    element: React.ReactNode;
};

const ProtectedRoute: React.FC<ProtectedRouteProps> = ({ element, ...rest }) => {
    const { isAuthenticated } = useContext(AuthContext);

    return isAuthenticated ? <>{element}</> : <Navigate to="/login" replace={true} />;
};

export default ProtectedRoute;
