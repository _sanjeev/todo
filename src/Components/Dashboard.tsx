import React, { useState } from "react";
import "../App.css";
import TodoForm from "./TodoForm";
import TodoList from "./TodoList";
import { TodoProvider } from "./TodoContext";

const Dashboard = () => {
    const [isModalOpen, setModalOpen] = useState<boolean>(false);
    return (
        <div>
            <TodoProvider>
                <div className="app">
                    <TodoForm isModalOpen={isModalOpen} setModalOpen={setModalOpen} />
                    <div>
                        <TodoList />
                        <button onClick={() => setModalOpen(true)}>Create Todo</button>
                    </div>
                </div>
            </TodoProvider>
        </div>
    );
};

export default Dashboard;
