import React, { useState, useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { AuthContext } from "../App";
import "../App.css";

type LoginProps = {
    setAuthenticated: React.Dispatch<React.SetStateAction<boolean>>;
};

const Login: React.FC<LoginProps> = ({ setAuthenticated }) => {
    const [email, setEmail] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const navigate = useNavigate();
    const { login } = useContext(AuthContext);

    const handleLogin = (e: React.FormEvent) => {
        e.preventDefault();
        const user = {
            email: "tom@gmail.com",
            password: "password",
        };

        if (email === user.email && password === user.password) {
            const token = "14c4cd7c-9404-464f-9037-4725cd375043";
            localStorage.setItem("token", token);
            login(user);
            setAuthenticated(true);
            navigate("/dashboard");
        } else {
            toast.error("Invalid credentials", {
                position: toast.POSITION.TOP_CENTER,
            });
        }
    };

    return (
        <>
            <ToastContainer />
            <div className="login">
                <h2>Login</h2>
                <form className="login__form" onSubmit={handleLogin}>
                    <div className="login__form-group">
                        <label htmlFor="email">Email:</label>
                        <input type="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)} required />
                    </div>
                    <div className="login__form-group">
                        <label htmlFor="password">Password:</label>
                        <input type="password" id="password" value={password} onChange={(e) => setPassword(e.target.value)} required />
                    </div>
                    <button type="submit" className="login__button">
                        Login
                    </button>
                </form>
                <p className="login__signup">
                    Don't have an account? <Link to="/signup">Sign up</Link>
                </p>
            </div>
        </>
    );
};

export default Login;
