import React, { createContext, useState, useContext } from "react";

type User = {
    email: string;
    name: string;
    // Add more properties as needed
};

type AuthContextType = {
    user: User | null;
    login: (user: User) => void;
    logout: () => void;
};

const initialAuthContext: AuthContextType = {
    user: null,
    login: () => {},
    logout: () => {},
};

export const AuthContext = createContext<AuthContextType>(initialAuthContext);

export const useAuth = () => useContext(AuthContext);
