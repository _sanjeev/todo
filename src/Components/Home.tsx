import React from "react";
import { Link } from "react-router-dom";
import "../App.css";

const Home = () => {
    return (
        <div className="home">
            <h1>Welcome to Todo App</h1>
            <p>Create and manage your todos with ease.</p>
            <Link to="/login" className="home__button">
                Get Started
            </Link>
        </div>
    );
};

export default Home;
