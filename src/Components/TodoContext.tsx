import React, { createContext, useState, useContext } from "react";

interface Todo {
    id: string;
    title: string;
    subTask: [];
    completed: boolean;
}

interface TodoContextType {
    todos: Todo[];
    addTodo: (todo: Todo) => void;
    updateTodo: (todo: Todo) => void;
}

const initialTodoContext: TodoContextType = {
    todos: [],
    addTodo: () => {},
    updateTodo: () => {},
};

type TodoProviderProps = {
    // Add any props here if needed
};

export const TodoContext = createContext<TodoContextType>(initialTodoContext);

export const TodoProvider: React.FC<React.PropsWithChildren<TodoProviderProps>> = ({ children }) => {
    const [todos, setTodos] = useState<Todo[]>([]);

    const addTodo = (todo: Todo) => {
        setTodos([...todos, todo]);
    };

    const updateTodo = (updatedTodo: Todo) => {
        setTodos((prevTodos) => {
            const updatedTodos = prevTodos.map((todo) => {
                if (todo.id === updatedTodo.id) {
                    return {
                        ...todo,
                        ...updatedTodo,
                    } as Todo;
                }
                return todo;
            });
            return updatedTodos;
        });
    };

    return <TodoContext.Provider value={{ todos, addTodo, updateTodo }}>{children}</TodoContext.Provider>;
};

export const useTodoContext = () => useContext(TodoContext);
