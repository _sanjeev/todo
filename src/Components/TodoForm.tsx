import React, { useState } from "react";
import { useTodoContext } from "./TodoContext";
import { v4 as uuid } from "uuid";
import Modal from "react-modal";
import "./TodoForm.css";

interface ModalState {
    // id: number;
    isModalOpen: boolean;
    setModalOpen: React.Dispatch<React.SetStateAction<boolean>>;
}

const TodoForm: React.FC<ModalState> = ({ isModalOpen, setModalOpen }) => {
    const [title, setTitle] = useState<string>("");
    const [subtask, setSubtask] = useState<string>("");

    const { addTodo } = useTodoContext();

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        if (title.trim() !== "") {
            const newTodo: any = {
                id: uuid(),
                title: title,
                subTask: [],
                completed: false,
            };
            addTodo(newTodo);
            setModalOpen(false);
        }
        setTitle("");
    };

    return (
        <Modal isOpen={isModalOpen} onRequestClose={() => setModalOpen(false)}>
            <div className="login">
                <h2>Create Todo</h2>
                <form className="login__form" onSubmit={handleSubmit}>
                    <div className="login__form-group">
                        <label htmlFor="title">Title:</label>
                        <input type="text" id="title" value={title} onChange={(e) => setTitle(e.target.value)} required />
                    </div>
                    <button type="submit" className="login__button">
                        Create
                    </button>
                </form>
            </div>
        </Modal>
    );
};

export default TodoForm;
